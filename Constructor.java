package basic.terminologies;


public class Constructor {
	
	//variables
	String name;
	int id;
	
	// Static block
    static
    {
        // Print statement
        System.out.print ("Static block can be printed without main method"+"\n");
    }
	
	 //This is the Default Constructor
	 Constructor()
	 {
	    //Constructor body
		 System.out.println("Default Constructor called");
	 }
	 
	 
	 //Creating a parameterized constructor
	 Constructor(String name, int id)
	 {
	     this.name = name;
	     this.id = id;
	 }

	//Main method
	 public static void main(String[] args) {
		 
	 		Constructor object=new Constructor();
	 		System.out.println(object.name);
	 		System.out.println(object.id+"\n");
	 		 
	 		Constructor object1 = new Constructor("Thanya", 26);
	 		System.out.println("Name: " + object1.name );
	 		System.out.println("id: " + object1.id+"\n");
	 		    
	 		Constructor object2 = new Constructor("Kajan", 34);
	 		System.out.println("Name: " + object2.name );
	 		System.out.println("id: " + object2.id+"\n");
	 		
	 }
}
